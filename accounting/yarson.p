/* YARSON.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/* Yilsonu islemleri yapar */
def var ii    as dec format ">>>,>>>,>>>,>>>,>>>".
def var jj    as dec format ">>>,>>>,>>>,>>>,>>>".
def var ii-tp as dec format ">>>,>>>,>>>,>>>,>>>".
def var jj-tp as dec format ">>>,>>>,>>>,>>>,>>>".
def var sirasay as int.
def var sonno as int.
def var ilksifre as char format "x(5)".
def var alsifre as char format "x(5)".
def workfile lafvar
      field lp-ana       as int format "999"
      field lp-alt1      as int format "999"
      field lp-alt2      as int format "99999"
      field lp-adi       as char format "x(37)"
      field lmiz-brc     as dec format ">>>,>>>,>>>,>>>,>>9"
      field lmiz-alc     as dec format ">>>,>>>,>>>,>>>,>>9".



      /* ----------- s t o p ---------------- */


update "Şifreyi gir.....:" ilksifre BLANK with frame ilk
       no-label centered row 5 title
       "Y ı l s o n u     İ ş l e m l e r i ".
     for first  muhprm.
     alsifre = sifre.
     end.
     if alsifre <> ilksifre then leave.

/*  stop. */

/* __program test edildi dogru calisiyor istenildiginde stop 'u kaldir ___ */
Message "Lütfen Bekleyiniz...".
for each muhpla where p-ana > 0:
ii = 0.
jj = 0.
for each muhyev use-index kebir-index where
	yana = p-ana and yalt1 = p-alt1 and  p-alt2 = yalt2.
ii = ii +  borc.
jj = jj + alacak.
end.
create lafvar.
lp-ana = p-ana.
lp-alt1 = p-alt1.
lp-alt2 = p-alt2.
lp-adi  = p-adi.
if ii > jj then
     do: lmiz-brc = ii - jj. lmiz-alc = 0. ii-tp = ii-tp + lmiz-brc. end.
if jj > ii then
     do: lmiz-alc = jj - ii. lmiz-brc = 0. jj-tp = jj-tp + lmiz-alc. end.
if ii = jj then do: lmiz-alc = 0 . lmiz-brc = 0.        end.
end.

disp " toplama bitti...> " ii-tp jj-tp with frame eef no-label
	   title "kontrol tablosu".
if ii-tp <> jj-tp then
		  /* ======== yevmiyede o hesap var  HESAP planinda yok ise = */
		  DO:
		  message "Hesap plani eksikligi var Bekleyiniz..".
		  message "Gerekli Hesap Planlarini yarat ve tekrar çalistir".
		  ii= 0 .
		  jj = 0.
		    for each muhyev.
		     find  muhpla where
			p-ana = yana and p-alt1 = yalt1 and p-alt2 = yalt2
			   no-error no-wait.
		      if not available muhpla then
			disp "hata" yana yalt1 yalt2 " nolu hesap plani yarat ."
			with frame iae no-label .
		     end. /* muhyev in end'i   */
		   pause.
		   leave.
		   END.  /* DO nun end'i  */
pause .001.
for last muhyev.
sonno = any1 + 1.
end.
   find muhyev where any1 = sonno and any2 = 0 no-error.
	if  not available muhyev then
	      DO:   /* ----  1202 no li do --- */
		create muhyev.
		any1 = sonno.
		any2 = 0.
		dgun = 31.
		dayi = 12.
		dyil = int(substring(string(today),7,2)) + 1900 .
		ba-kod = "0".
		update "Kapaniş kaydi Yevmiye.Tarh:"
		dgun auto-return
		dayi auto-return
		dyil auto-return       skip
		"Kapaniş Gen açiklama:" acik   with frame acikgir12
		row 15 column 1  no-labels     overlay
		title " Genel açiklama ve yevmiye kaydi girişi".
	      END.

	    /* ---- aciklamaya 0 girme --- */

	    find muhack where a-any1 = sonno no-error.
	    if not available muhack then
	       do:
		 create muhack.
		 a-any1 = sonno.
		 a-acik = input acik.
		 a-gun = input dgun.
		 a-ayi = input dayi.
		 a-yil = input dyil.
		 assign a-any1 = sonno.
	       end.  /* not available thru nin END i */


/* ----------- alacak toplamlarini BORC hanaseni atar ------ */
message "Borc Basladi Lütfen bekleyiniz ".
for each lafvar where lmiz-alc >  0:
sirasay = sirasay + 1.
   find muhyev where any1 = sonno and any2 = sirasay no-error.
	if  not available muhyev then
	      DO:   /* ----  1202 no li do --- */
		create muhyev.
		any1 = sonno.
		any2 = sirasay.
		dgun = input dgun.
		dayi = input dayi.
		dyil = input dyil.
		acik = input acik.
		ba-kod = "1".
		BORC = LMIZ-ALC.
		Alacak = 0.
		yana = lp-ana.
		yalt1 = lp-alt1.
		yalt2 = lp-alt2.
		assign any1 = sonno any2 = sirasay.
	      END.
end.
message " alacak işl basladi lütfen bekleyiniz ....".
    for each lafvar where lmiz-brc > 0:
    sirasay = sirasay + 1.
     find muhyev where any1 = sonno and any2 = sirasay no-error.
	if  not available muhyev then
	      DO:   /* ----  1202 no li do --- */
		create muhyev.
		any1 = sonno.
		any2 = sirasay.
		dgun = input dgun.
		dayi = input dayi.
		dyil = input dyil.
		acik = input acik.
		ba-kod = "2".
		BORC = 0.
		Alacak = LMIZ-BRC.
		yana  = lp-ana.
		yalt1 = lp-alt1.
		yalt2  = lp-alt2.
		assign any1 = sonno any2 = sirasay.
	      END.

end.

Message "İşleminiz basari ile sonuclandi ".
pause.
