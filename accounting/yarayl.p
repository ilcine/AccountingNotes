/* YARAYL.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/* aylik kapama yapar */
def var aa as int format "99".
update  "Hangi Ayı Kapatacaksınız.....:" aa with frame ilk
       no-label centered row 10 title "Ay kapatma İşlemleri ".

for each muhack where a-ayi = aa:
a-kod = "*".
end.
display aa ".ay kapatildi artık bu ay ile işlem yapamazsınız"   skip
	   " eger yapılması gerekiyorsa yevmiye girişlerinde"  skip
	   "  sizden şifre soracaktır "
	   with frame abi no-label centered.
message "Enter ile Menuye donünüz".
pause 15.
